#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=1:00:00
#SBATCH --job-name=bedtoolsCAGE
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/output_bedtoolsCAGE_%j.o
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_bedtoolsCAGE_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Analysis/BEDTools/2.29.2;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/other

#find overlaps with polyA tail and CAGE peaks
#bedtools window -w 100 -sm -a merged_transcripts.bed -b polya_formatted.bed
#bedtools window -w 100 -sm -a merged_transcripts.bed -b CAGEpeaks.bed

#find intergenic regions (only NON-overlapping with annotated protein coding genes)
bedtools window -w 100 -sm -v -a merged_transcripts.bed -b protcoding.bed