#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=25000M
#SBATCH --time=24:00:00
#SBATCH --job-name=samtools_sort
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/output_fastqc_%j.e
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_fastqc_%j.e
#SBATCH --mail-type=end,fail

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/dta_hisat2

module add UHTS/Analysis/samtools/1.10;

for i in $(seq 0 11); do samtools sort -@ 4 -o $i.sorted.bam $i.sam; done

# samtools sort [-l level] [-u] [-m maxMem] [-o out.bam] [-O format] [-M] [-K kmerLen] [-n] [-t tag] [-T tmpprefix] [-@ threads] [in.sam|in.bam|in.cram]