#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=1:00:00
#SBATCH --job-name=cpat
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_cpat_%j.e
#SBATCH --mail-type=end,fail

module add SequenceAnalysis/GenePrediction/cpat/1.2.4;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/other

cpat.py -g /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto/exons.fa -o out.cpat -x Human_Hexamer.tsv -d Human_logitModel.RData