#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=1:00:00
#SBATCH --job-name=kallisto_index
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_kallistoindex_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Assembler/cufflinks/2.2.1;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto

gunzip < /data/courses/rnaseq/lncRNAs/Project2/references/genome_sequence/GRCh38.primary_assembly.genome.fa.gz > GRCh38.assembly.fa

gffread /data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/merged.gtf -w exons.fa -g GRCh38.assembly.fa

rm GRCh38.assembly.fa