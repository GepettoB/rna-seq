## RNA-Seq lncRNA Project 2 - Thomas Behrens
Cluster Working Directory: /data/courses/rnaseq/lncRNAs/Project2/tbehrens/

## Notes
* Many scripts are somewhat fragmented so that they could be run individually for testing, but could easily be merged (such as fastqc and multiqc).
* Some scripts were used multiple times for similar functions, only changing file names (ex. conv2bed.sh), and so don't have explicit examples of each case.
* sleuth.R script should not be run all at once. Consists of several sections with different purposes, as well as certain lines that differ between gene- and transcript-level differential expression analysis.

## Script Details
Cluster Jobs (in order of workflow):
* **fqc.sh** - fastqc of individual fastq files  
* **multiqc.sh** - summarizing results of all fastqc tests  
* **indexing.sh** - indexing step for hisat2, based on reference genome  
* **rf_hisat.sh** - hisat2 read mapping to reference genome  
* **sam_sort.sh** - sorting sam files (required for StringTie) and converting to bam format (to save space)  
* **assembly.sh** - StringTie transcriptome assembly  
* **stringtie_merge.sh** - merging assembly results into one meta-assembly in GTF format  
* **gffread.sh** - creating a fasta based on the reference and meta-assembly
* **kallisto_prep.sh** - indexing step for Kallisto
* **kallisto_quant.sh** - quantifying transcript expression with Kallisto
* **conv2bed.sh** - used to convert a gtf's to bed format. Used during Kallisto step, and for CAGE peaks, polyA sites, and protein coding genes, and for converting meta-assembly to bed format. Note that the only difference is file name, and so only one example is included in the script
* **bedtools_CAGE.sh** - finding overlaps between bed files with a window of 100 nucleotides. Used for CAGE peaks, polyA sites, and protein coding genes (to identify intergenic)
* **cpat.sh** - CPAT to calculate protein coding potential of transcripts

Non-Cluster Scripts:
* **assembly_details.sh** - BASH commands for counting (novel) transcripts, genes, and exons from meta-assembly GTF. Also includes the results as comments.
* **sleuth.R** - differential expression analysis using R, and connected analysis, including creating plots and merging in data from integrative analysis
* **data_merging.R** - R script mostly for put data in one place. Mostly cbind and merge commands
* **Other_Scripts.txt** - Single line commands for filtering

Less Relevant:
* **out.cpat.r** - output script from CPAT. Uses **Human_logitModel.RData** for calculation
* **label_table.txt** - table coinciding directories to subtypes for sleuth
* **gtflist.txt** - list of gtf files. Used by some scripts to iterate over
