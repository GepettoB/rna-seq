#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=1:00:00
#SBATCH --job-name=convert2bed
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_convert2bed_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Analysis/bedops/2.4.40;

#cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto

#convert2bed --input=GTF < /data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/merged.gtf > /data/courses/rnaseq/lncRNAs/Project2/tbehrens/merged.bed



cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens
convert2bed --input=GTF < /data/courses/rnaseq/lncRNAs/Project2/tbehrens/protcoding.gtf > /data/courses/rnaseq/lncRNAs/Project2/tbehrens/protcoding.bed