#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000M
#SBATCH --time=6:00:00
#SBATCH --job-name=stringie_assembly
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/output_stringtie_%j.e
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/error_stringtie_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Aligner/stringtie/1.3.3b;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/dta_hisat2

# stringtie -o /data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/0.gtf -rf 0.sorted.bam

ANNOTATION='/data/courses/rnaseq/lncRNAs/Project2/references/annotations'

gunzip < $ANNOTATION/gencode.v38.annotation.gtf.gz > $ANNOTATION/gencode.v38.annotation.gtf

for i in $(seq 0 11); do stringtie $[i].sorted.bam -o /data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/$[i].gtf -p 1 -G $ANNOTATION/gencode.v38.annotation.gtf --rf; done

rm $ANNOTATION/gencode.v38.annotation.gtf