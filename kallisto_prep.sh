#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=1:00:00
#SBATCH --job-name=kallisto_index
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_kallistoindex_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Analysis/kallisto/0.46.0;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto

kallisto index -i kallisto.index exons.fa