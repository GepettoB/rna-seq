#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=12:00:00
#SBATCH --job-name=kallisto_index
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_kallistoquant_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Analysis/kallisto/0.46.0;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto

/data/courses/rnaseq/lncRNAs/Project2/tbehrens/fastqc

FORWARD=(/data/courses/rnaseq/lncRNAs/Project2/fastq/*R1*.gz)
REVERSE=(/data/courses/rnaseq/lncRNAs/Project2/fastq/*R2*.gz)

for i in $(seq 0 11); do kallisto quant -i kallisto.index -o /data/courses/rnaseq/lncRNAs/Project2/tbehrens/kallisto/$[i]out --rf-stranded -b 10 ${FORWARD[$i]} ${REVERSE[$i]}; done