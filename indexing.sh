#!/usr/bin/env bash

#SBATCH --cpus-per-task=3
#SBATCH --mem-per-cpu=4000M
#SBATCH --time=03:00:00
#SBATCH --job-name=hisat2_index
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/output_index_%j.e
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/error_index_%j.e
#SBATCH --mail-type=end,fail

cd /data/courses/rnaseq/lncRNAs/Project2/references/hisat_index_gencode

# /data/courses/rnaseq/lncRNAs/Project2/fastq/

module add UHTS/Aligner/hisat/2.2.1;

# unzip reference genome
# zcat /data/courses/rnaseq/lncRNAs/Project2/references/genome_sequence/GCF_000001405.26_GRCh38_genomic.fna.gz > /data/users/tbehrens/hisat2/GCF_000001405.26_GRCh38_genomic.fna

gunzip < /data/courses/rnaseq/lncRNAs/Project2/references/genome_sequence/GRCh38.primary_assembly.genome.fa.gz > GRCh38.primary_assembly.genome.fa

# create index from genome
hisat2-build -p 3 -f GRCh38.primary_assembly.genome.fa hisat2_index

rm GRCh38.primary_assembly.genome.fa