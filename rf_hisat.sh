#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=24:00:00
#SBATCH --job-name=hisat2_mapping
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/dta_hisat2/error_rfhisat_%j.e
#SBATCH --mail-type=end,fail

cd /data/courses/rnaseq/lncRNAs/Project2/references/hisat_index_gencode

module add UHTS/Aligner/hisat/2.2.1;

### Alternate version -> but would output all to the same SAM file?
### create two lists of files, for forward and reverse reads
### LISTF=`ls /data/courses/rnaseq/lncRNAs/Project2/fastq/ | grep R1 | paste -s -d, -`
### LISTR=`ls /data/courses/rnaseq/lncRNAs/Project2/fastq/ | grep R2 | paste -s -d, -`

### run through lists of reads
### hisat2 -q -p 4 -x hisat2_index -1 $LISTF -2 $LISTR -S 

# testing one pair of reads
# hisat2 -q -p 4 -x hisat2_index -1 /data/courses/rnaseq/lncRNAs/Project2/fastq/1_1_L3_R1_001_ij43KLkHk1vK.fastq.gz -2 /data/courses/rnaseq/lncRNAs/Project2/fastq/1_1_L3_R2_001_qyjToP2TB6N7.fastq.gz -S /data/users/tbehrens/hisat2/1_1_BAM

# create two arrays, for forward and reverse reads
FORWARD=(/data/courses/rnaseq/lncRNAs/Project2/fastq/*R1*.gz)
REVERSE=(/data/courses/rnaseq/lncRNAs/Project2/fastq/*R2*.gz)

for i in $(seq 0 11); do hisat2 -q -p 4 --dta --rna-strandness 'RF' -x hisat2_index -1 ${FORWARD[$i]} -2 ${REVERSE[$i]} -S /data/courses/rnaseq/lncRNAs/Project2/tbehrens/dta_hisat2/index_$[i] 2> /data/courses/rnaseq/lncRNAs/Project2/tbehrens/dta_hisat2/summaries/index_$[i].e; done
