#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000M
#SBATCH --time=01:00:00
#SBATCH --job-name=fastqc_1
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/multiqc_%j.o
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/multiqc_%j.e

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/fastqc/

module add UHTS/Analysis/MultiQC/1.8

multiqc .