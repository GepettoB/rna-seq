#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=1000M
#SBATCH --time=6:00:00
#SBATCH --job-name=stringtie_merge
#SBATCH --output=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/output_merge_%j.e
#SBATCH --error=/data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie/error_merge_%j.e
#SBATCH --mail-type=end,fail

module add UHTS/Aligner/stringtie/1.3.3b;

cd /data/courses/rnaseq/lncRNAs/Project2/tbehrens/stringtie

#unzip annotations, remove at end
gunzip < /data/courses/rnaseq/lncRNAs/Project2/references/annotations/gencode.v38.annotation.gtf.gz > gencode_annotation.gtf

#include reference annotations, output name, and thread count
stringtie --merge -G gencode_annotation.gtf -o merged.gtf -p 1 --rf /data/courses/rnaseq/lncRNAs/Project2/tbehrens/scripts/gtflist.txt

rm annotation.gtf