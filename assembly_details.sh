#!/bin/bash

echo 'Number of exons:' `awk '$3=="exon"' merged.gtf | wc -l`
# 1706360

echo 'Number of transcripts:' `awk '$3=="transcript"' merged.gtf | wc -l`
# 257282

echo 'Number of genes:' `awk '$3=="transcript"' merged.gtf | cut -f9 | cut -d ' ' -f2 | uniq | wc -l`
# 60443

echo 'Novel Genes:' `awk '$3=="transcript"' merged.gtf | cut -f9 | cut -d ' ' -f4 | grep 'MSTRG' | tr -d ';"' | cut -d '.' -f2 | sort | uniq | wc -l`
# 8331

echo 'Single exon transcripts:' `cut -f3 merged.gtf | uniq -c | grep ' 1 exon' | wc -l`
# 25891

echo 'Single exon genes:' `cut -f9 merged.gtf | cut -f4 -d' ' | uniq -c | grep ' 2 "MSTRG' | wc -l`
# 850

#Boris's Results:
# exons 1.6m
# transcripts 252k
# genes 51k
# 15.8k
# 26k