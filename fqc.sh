#!/usr/bin/env bash

#SBATCH --cpus-per-task=3
#SBATCH --mem-per-cpu=1000M
#SBATCH --time=01:00:00
#SBATCH --job-name=fastqc_1
#SBATCH --output=/home/tbehrens/output_fastqc_%j.o
#SBATCH --error=/home/tbehrens/error_fastqc_%j.e
#SBATCH --mail-user=thomas.behrens@students.unibe.ch
#SBATCH --mail-type=end,fail

cd /data/courses/rnaseq/lncRNAs/Project2/fastq/

module add UHTS/Quality_control/fastqc/0.11.9;

module load fastqc

fastqc -t 3 -o /home/tbehrens/fastqc *.fastq.gz